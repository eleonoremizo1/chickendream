/*****************************************************************************

        Approx_transc.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_Approx_transc_CODEHEADER_INCLUDED)
#define fstb_Approx_transc_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/bit_cast.h"
#include "fstb/fnc.h"
#include "fstb/BitEquiv.h"
#include "fstb/Poly.h"
#include "fstb/Vs32.h"
#include "fstb/Vx32_conv.hpp"

#if defined (_MSC_VER)
	#include <intrin.h>
#endif

#include <limits>
#include <type_traits>

#include <cassert>



namespace fstb
{



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// For some reasons, Clang complains if the following functions are not
// defined before they are called.

// C1 continuity
template <typename T>
constexpr T	Approx::log2_poly2c1 (T x) noexcept
{
	return Poly::horner (x, T (-5.f / 3), T (2.f), T (-1.f / 3));
}



/*
C4 continuity
Coefficients by Z1202
https://www.kvraudio.com/forum/viewtopic.php?p=7319721#p7319721
*/

template <typename T>
constexpr T	Approx::log2_poly5c4 (T x) noexcept
{
	return Poly::estrin (x,
		T (-1819 / 651.0),
		T (5),
		T (-10 / 3.0),
		T (10 / 7.0),
		T (-1 / 3.0),
		T (1 / 31.0)
	);
}



/*
n=50
FindFit[Table[
  Log[x]/Log[2], {x, 1, 2, 1/n}], {c0 + c1 (1 + (x - 1)/n) + 
   c2 (1 + (x - 1)/n)^2 + c3 (1 + (x - 1)/n)^3 + 
   c4 (1 + (x - 1)/n)^4 + 
   c5 (1 + (x - 1)/n)^5, {c0 + c1 + c2 + c3 + c4 + c5 == 0, 
   c0 + 2 c1 + 4 c2 + 8 c3 + 16 c4 + 32 c5 == 1, 
   c1 + 2 c2 + 3 c3 + 4 c4 + 5 c5 == 
    2 (c1 + 4 c2 + 12 c3 + 24 c4 + 80 c5)}}, {c0, c1, c2, c3, c4, c5},
  x]
*/

template <typename T>
constexpr T	Approx::log2_poly5 (T x) noexcept
{
	return Poly::estrin (x,
		T (-2.4395118595618),
		T ( 3.80998968934317),
		T (-1.75998771172059),
		T ( 0.40029024875655),
		T (-0.000133317241258202),
		T (-0.0106470495760765)
	);
}



/*
n=10000
FindFit[
	Table[Log[x + 10^-50 + 1]/((x + 10^-50) Log[2]), {x, 0, 1, 1/n}],
	{	  1/Log[2] + c1 ((x - 1)/n) + c2 ((x - 1)/n)^2
		+ c3 ((x - 1)/n)^3 + c4 ((x - 1)/n)^4
		+ c5 ((x - 1)/n)^5 + c6 ((x - 1)/n)^6,
		{	1/Log[2] + c1 + c2 + c3 + c4 + c5 + c6 == 1, 
			1/Log[2] + 2 c1 + 3 c2 + 4 c3 + 5 c4 + 6 c5 + 7 c6 == 0.5/Log[2] }
	}, {c1, c2, c3, c4, c5, c6}, x
]
*/

template <typename T>
constexpr T	Approx::log2_poly7 (T x) noexcept
{
	x -= T (1);
	return x * Poly::estrin (x,
		T ( 1 / fstb::LN2),
		T (-0.7211387950779122),
		T ( 0.4771378300542432),
		T (-0.3362788591112742),
		T ( 0.2091714453949908),
		T (-0.0898814240618324),
		T ( 0.01829476191282143)
	);
}



// Quadratic approximation of 2^x in [0 ; 1]
template <typename T>
constexpr T	Approx::exp2_poly2 (T x) noexcept
{
	return Poly::horner (x, T (1), T (2.0 / 3.0), T (1.0 / 3.0));
}



/*
!!! approximation of 2^x in [-0.5 ; 0.5] !!!
C2 continuity
Relative error below 2.52e-6
Coefficients found by Andrew Simper
https://www.kvraudio.com/forum/viewtopic.php?p=7675486#p7675486
*/

template <typename T>
constexpr T	Approx::exp2_poly5c2 (T x) noexcept
{
	return Poly::estrin (x,
		T (1.000000237),
		T (0.69314655 ),
		T (0.24021519 ),
		T (0.05550965 ),
		T (0.00969821 ),
		T (0.00132508 )
	);
}



/*
C0 continuity
Relative error below 1e-7
Coefficients found by Andrew Simper
https://www.kvraudio.com/forum/viewtopic.php?p=7677357#p7677357
*/

template <typename T>
constexpr T	Approx::exp2_poly5 (T x) noexcept
{
	return Poly::estrin (x,
		T (1               ),
		T (0.69315168779795),
		T (0.2401596780318 ),
		T (0.055817593635  ),
		T (0.008992164746  ),
		T (0.001878875789  )
	);
}



/*
C1 continuity
Relative error below 1e-10
Coefficients found by Andrew Simper
https://discord.com/channels/507604115854065674/507630527847596046/1073792844852109354
Currently unused
*/

template <typename T>
constexpr T	Approx::exp2_poly7c1 (T x) noexcept
{
	return Poly::estrin (x,
		T (1                ),
		T (0.693147180559945),
		T (0.2402264513153  ),
		T (0.055504846557   ),
		T (0.00961452393    ),
		T (0.00134189347    ),
		T (0.00014355398    ),
		T (0.000021550183   )
	);
}



/*
C2 continuity
Relative error below 4e-10
Coefficients found by Andrew Simper
https://www.kvraudio.com/forum/viewtopic.php?p=7677357#p7677357
*/

template <typename T>
constexpr T	Approx::exp2_poly7c2 (T x) noexcept
{
	return Poly::estrin (x,
		T (1                 ),
		T (0.693147180559945 ),
		T (0.2402265069591007),
		T (0.0555044941070   ),
		T (0.009615262656    ),
		T (0.001341316600    ),
		T (0.000143623130    ),
		T (0.000021615988    )
	);
}



/*
C0 continuity
Relative error below 1e-8
Coefficients found by Andrew Simper
https://discord.com/channels/507604115854065674/507630527847596046/1073401190752211044
Currently unused
*/

template <typename T>
constexpr T	Approx::exp2_frac32 (T x) noexcept
{
	const auto     num = Poly::horner (x,
		T ( 1                       ),
		T ( 0.43001011963406321481  ),
		T ( 0.078781691204622431024 ),
		T ( 0.0068412398808644541218)
	);
	const auto     den = Poly::horner (x,
		T ( 1                       ),
		T (-0.26313747880277266189  ),
		T ( 0.020954004157248858065 )
	);

	return num / den;
}



/*
C1 continuity
Relative error below 3e-10
Coefficients found by Andrew Simper
https://discord.com/channels/507604115854065674/507630527847596046/1073399579254472785
Currently unused
*/

template <typename T>
constexpr T	Approx::exp2_frac33 (T x) noexcept
{
	const auto     num = Poly::horner (x,
		T ( 1                       ),
		T ( 0.35857922101497249378  ), // Fixed, was: 0.35857922102065181713
		T ( 0.052334448171287628253 ), // Fixed, was: 0.052334448175807304904
		T ( 0.0033039755853799162091)
	);
	const auto     den = Poly::horner (x,
		T ( 1                       ),
		T (-0.33456795953679829836  ),
		T ( 0.044012850754226687244 ),
		T (-0.0023360688316083697630)  // Fixed, was: -0.0023360688316084363657
	);

	return num / den;
}



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// C1 continuity
template <typename T>
T	Approx::log2 (T val) noexcept
{
	return log2_base (val, log2_poly2c1 <T>);
}



/*
C1 continuity
Max error: 1.8e-4
*/

template <typename T>
T	Approx::log2_5th (T val) noexcept
{
	return log2_base (val, log2_poly5 <T>);
}



/*
C1 continuity, exact 1st derivative at 2^N
Max error: 2.69e-6
*/

template <typename T>
T	Approx::log2_7th (T val) noexcept
{
	return log2_base (val, log2_poly7 <T>);
}



/*
Piece-wise linear approximation
log2_crude (1) != 0
Max error (absolute): 0.44 %
Implemented as an inverted exp2_crude() with a refined constant c.
c = 0: log2_crude (1) == 0
c = 480709 (float), 60088.6   (double): best RMS error
c = 361010 (float), 45126.165 (double): best maximum error
*/

template <typename T>
T	Approx::log2_crude (T val) noexcept
{
	assert (val > T (0));

	typedef BitEquiv <T> BE;
	typedef typename BE::Int TI;

#if 1

	typedef typename BE::ScalInt SI;
	typedef typename BE::ScalFlt SF;
	constexpr auto c =
		SI ((BE::_bits == 64) ? 45126.165 * fstb::TWOP32 : 361010);
	constexpr auto b =
		SI ((BE::_bits == 64) ? 0x3FF00000 : 0x3F800000) << (BE::_bits - 32);
	const T        ar { SF (1) / (int64_t (1) << (std::numeric_limits <SF>::digits - 1)) };
	const TI       d  { b - c };

	const auto     u = fstb::bit_cast <TI> (val);
	const auto     y = T (u - d) * ar;

	return y;

#else

	/*
	Old version, subject to significant quantization (staircase effects)
	Don't use any multiply.
	Warning, log2_crude (1) != 0
	Errors:
	RMS:  0.0236228
	mean: -0.0145537
	min:  -0.0430415 @ 0.180279
	max:  0.0430603 @ 0.125
	Source: Evan Balster
	https://github.com/romeric/fastapprox/pull/5
	*/
	static_assert (BE::_bits == 32, "Not designed for double");
	const auto     mask = TI (0x43800000);
	const auto     d    = T (0x1.7EF4FAp+8f); // 382.95695f;

	auto           i    = fstb::bit_cast <TI> (val);
	i    = mask | (i >> 8);
	val  = fstb::bit_cast <T> (i);
	val -= d;

	return val;

#endif
}



/*
Implemented as (log2_crude (x) - log2_crude (1 / x)) / 2
log2_crude (1) == 0
Max error (absolute): 3.81e-3
*/

template <typename T>
T	Approx::log2_crude2 (T val) noexcept
{
	assert (val > T (0));

	typedef BitEquiv <T> BE;
	typedef typename BE::Int TI;
	typedef typename BE::ScalFlt SF;
	const T        arh {
		SF (1) / (int64_t (1) << (std::numeric_limits <SF>::digits))
	};

	T              v_r;
	if constexpr (! std::is_scalar_v <T>)
	{	// Vectors
#if fstb_ARCHI == fstb_ARCHI_ARM
		v_r = val.rcp_approx (); 
#else
		// rcp_approx() on SSE would increase the error max to 3.97e-3
		v_r = val.rcp_approx2 ();
#endif
	}
	else
	{
		v_r = T (1) / val;
	}
	const auto     up = fstb::bit_cast <TI> (val);
	const auto     un = fstb::bit_cast <TI> (v_r);
	const auto     y  = T (up - un) * arh;

	return y;
}



// C1 continuity
template <typename T>
T	Approx::exp2 (T val) noexcept
{
	return exp2_base (val, exp2_poly2 <T>);
}



/*
Max error on [0; 1]: 2.44e-7.
Measured on [-20; +20]: 8.15e-7
*/

template <typename T>
T	Approx::exp2_5th (T val) noexcept
{
	return exp2_base (val, exp2_poly5 <T>);
}



/*
Max error on [0; 1]: 1.64e-7
Measured on [-20; +20]: 7.91e-7
*/

template <typename T>
T	Approx::exp2_7th (T val) noexcept
{
	return exp2_base (val, exp2_poly7c2 <T>);
}



/*
Piece-wise linear approximation
Warning, exp2_crude (0) != 1
Max relative error: 3 %
The c constant may be tweaked for different error optimisations
c = 0: exp2_crude (0) == 1
c = 486400 (float), 60801.485 (double): best RMS error
c = 366393 (float), 45799.125 (double): best maximum error
Source:
Nicol N. Schraudolph,
A Fast, Compact Approximation of the Exponential Function,
Neural Computation 11, pp. 853-862, 1999
Modification: attempt to reduce the quantization (staircase effect).
*/

template <typename T>
T	Approx::exp2_crude (T val) noexcept
{
	typedef BitEquiv <T> BE;
	typedef typename BE::Int TI;

#if 1

	typedef typename BE::ScalInt SI;
	typedef typename BE::ScalFlt SF;
	constexpr auto c =
		SI ((BE::_bits == 64) ? 45799.125 * fstb::TWOP32 : 366393);
	constexpr auto b =
		SI ((BE::_bits == 64) ? 0x3FF00000 : 0x3F800000) << (BE::_bits - 32);
	const T        a { SI (1) << (std::numeric_limits <SF>::digits - 1) };
	const TI       d { b - c };

	const auto     u = TI (val * a) + d;
	const auto     y = fstb::bit_cast <T> (u);

	return y;

#else

	/*
	Previous version, subject to significant quantization (staircase effects)
	Don't use any multiply.
	Warning, exp2_crude (0) != 1
	Errors:
	RMS:  0.0204659
	mean: 0.0103297
	min:  -0.0294163 @ 1.04308
	max:  0.0302728 @ 1.48549
	Source: Evan Balster
	https://github.com/romeric/fastapprox/pull/5
	*/
	static_assert (BE::_bits == 32, "Not designed for double");
	const auto     c8 = TI (0x43808000);
	const auto     d  = T (0x1.7EF4FAp+8f); // 382.95695f;
	const auto     m  = TI (0x7FFFFF00);

	auto           i  = bit_cast <TI> (val + d);
	if constexpr (! std::is_scalar_v <T>)
	{
		i &= (i >= c8); // Vectors are handled differently
		i  = (i << 8) & m;
	}
	else
	{
		i = ((((i < c8) ? 0 : i) << 8) & m);
	}
	val = fstb::bit_cast <T> (i);

	return val;

#endif
}



/*
Implemented as exp2_crude (x / 2) / exp2_crude (-x / 2)
exp2_crude2 (0) == 1
Max relative error: 6.3e-3
c = 215300 (float), 26909.95  (double): best RMS error
c = 139156 (float), 17394.352 (double): best maximum error
*/

template <typename T>
T	Approx::exp2_crude2 (T val) noexcept
{
	typedef BitEquiv <T> BE;
	typedef typename BE::Int TI;
	typedef typename BE::ScalInt SI;
	typedef typename BE::ScalFlt SF;
	constexpr auto c =
		SI ((BE::_bits == 64) ? 17394.352 * fstb::TWOP32 : 139156);
	constexpr auto b =
		SI ((BE::_bits == 64) ? 0x3FF00000 : 0x3F800000) << (BE::_bits - 32);
	const T        ah { SI (1) << (std::numeric_limits <SF>::digits - 2) };
	const TI       d  { b - c };

	const auto     vi = TI (val * ah);
	const auto     un = d + vi;
	const auto     ud = d - vi;
	const auto     yn = fstb::bit_cast <T> (un);
	const auto     yd = fstb::bit_cast <T> (ud);
	if constexpr (! std::is_scalar_v <T>)
	{
		return yn.div_approx (yd); // Vectors
	}
	else
	{
		return yn / yd;
	}
}



/*
Approximation is good close to 0, but diverges for larger absolute values.
A is the approximation accuracy (the bigger, the larger the valid range).
A = 10 is a good start, maximum relative error :
6e-4  on [-1   ; 1  ]
0.2 % on [-2   ; 2  ]
1 %   on [-4.5 ; 4.5]
*/

template <int A, typename T>
constexpr T	Approx::exp_m (T val) noexcept
{
	static_assert (A > 0, "A must be strictly positive");
	static_assert (A <= 16, "A is too large, precision will suffer.");

	const int      p = 1 << A;
	const T        x = T (1) + val * T (1.f / p);

	return ipowpc <p> (x);
}



/*
Crude approximation, a > 0
Errors vary mostly with the absolute value of b
Source: Martin Leitner-Ankerl, Edward Kmett
https://github.com/ekmett/approximate/blob/master/cbits/fast.c
https://martin.ankerl.com/2007/10/04/optimized-pow-approximation-for-java-and-c-c/
T is float, double or Vf32
*/

template <typename T>
T	Approx::pow_crude (T a, T b) noexcept
{
	assert (a > T (0));

	typedef BitEquiv <T> BE;
	typedef typename BE::Int TI;
	typedef typename BE::ScalInt SI;

	constexpr auto c =
		SI ((BE::_bits == 64) ? 0x3FEF127F : 0x3F7893F5) << (BE::_bits - 32);
	auto           u = fstb::bit_cast <TI> (a);
	const auto     d = T (u - TI (c));
	u = TI (b * d + T (c));

	return fstb::bit_cast <T> (u);
}



// Max relative error: 0.073
template <typename T>
T	Approx::pow_3 (T a, T b) noexcept
{
	assert (a > T (0));

	const auto     l2a = log2 (a);
	const auto     y   = exp2 (b * l2a);

	return y;
}

// Max relative error: 1.26e-3
template <typename T>
T	Approx::pow_5 (T a, T b) noexcept
{
	assert (a > T (0));

	const auto     l2a = log2_5th (a);
	const auto     y   = exp2_5th (b * l2a);

	return y;
}

// Max relative error: 1.66e-5
template <typename T>
T	Approx::pow_7 (T a, T b) noexcept
{
	assert (a > T (0));

	const auto     l2a = log2_7th (a);
	const auto     y   = exp2_7th (b * l2a);

	return y;
}



/*
==============================================================================
Name: fast_partial_exp2_int_16_to_int_32
Description:
	Compute a fixed-point approximation of the following function :
	[0 ; 1[ -> [0.5 ; 1[
	  f : x -> 2^(x-1)
	The approximation uses the following polynomial :
	f(x) = (((x+1)^2) + 2) / 6
Input parameters :
	- val: Unsigned linear input, Fixed-point 0:16 data. Integer part
		is automatically discarded.
Returns: Unsigned exponential output, fixed-point 0:32 data, range is
	[0x80000000 ; 0xFFFFFFFF]
Throws: Nothing
==============================================================================
*/

uint32_t	Approx::fast_partial_exp2_int_16_to_int_32 (int val) noexcept
{
	const uint32_t c2 = 1431655766U;

#if defined (_MSC_VER)

	const uint32_t c0 = 0x80000000U;
	const uint32_t c1 = 2863311531U;

	uint32_t       result = val;
	result <<= 15;
	result  |= c0;
	result   = uint32_t (__emulu (result, result) >> 32);
	result   = uint32_t (__emulu (result,     c1) >> 32);
	result  += c2;

#else

	const int      resol = 16;
	const uint32_t mask  = (1 << resol) - 1;

	val &= mask;
	int64_t        step_64 (val + (1 << resol));
	step_64 *= step_64;
	step_64 *= (1 << (62 - (resol + 1) * 2)) / 3 + 1820;
	uint32_t       result = uint32_t (step_64 >> (62 - 32 - 1));
	result += c2;

#endif

	assert ((result & 0x80000000U) != 0);

	return result;
}



/*
Way more accurate (error < 8e-5 -> ~0.08 cent)
Polynomial approximation by Olli Niemitalo
y = (((1/150 * x + 2/75) * x + 3/25) * x + 26/75) * x + 0.5
*/

uint32_t	Approx::fast_partial_exp2_int_16_to_int_32_4th (int val) noexcept
{
	const uint32_t c0 = 0x80000000U;
	const uint32_t c1 = 0x58BF258C;
	const uint32_t c2 = 0x1EB851EB;
	const uint32_t c3 = 0x06D3A06D;
	const uint32_t c4 = 0x01B4E81B;

#if defined (_MSC_VER)

	const uint32_t x_32 = uint32_t (val) << 16;
	uint32_t       result = uint32_t (__emulu (c4,     x_32) >> 32);
	result += c3;
	result  = uint32_t (__emulu (result, x_32) >> 32);
	result += c2;
	result  = uint32_t (__emulu (result, x_32) >> 32);
	result += c1;
	result  = uint32_t (__emulu (result, x_32) >> 32);
	result += c0;

#else

	const uint32_t x_32 = uint32_t (val) << 16;
	uint32_t       result  = uint32_t ((c4 * uint64_t (x_32)) >> 32);
	result += c3;
	result  = uint32_t ((result * uint64_t (x_32)) >> 32);
	result += c2;
	result  = uint32_t ((result * uint64_t (x_32)) >> 32);
	result += c1;
	result  = uint32_t ((result * uint64_t (x_32)) >> 32);
	result += c0;

#endif

	assert ((result & 0x80000000U) != 0);

	return (result);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename T, typename P>
T	Approx::log2_base (T val, P poly) noexcept
{
	assert (val > T (0));

	if constexpr (! std::is_scalar_v <T>)
	{
		return val.log2_base (poly);
	}

	else
	{
		static_assert (std::is_floating_point_v <T>, "Floating point only");

		typedef std::make_unsigned_t <typename BitEquiv <T>::Int> TU;
		constexpr auto mdig  = TU (std::numeric_limits <T>::digits - 1);
		constexpr auto rexp  = sizeof (T) * CHAR_BIT - (mdig + 1);
		constexpr auto emsk  = TU ((1 <<  rexp     ) - 1);
		constexpr auto bias  = TU ((1 << (rexp - 1)) - 1);

		auto           x     = fstb::bit_cast <TU> (val);
		const auto     log_2 = int (x >> mdig) - int (bias);
		x  &= ~(emsk << mdig);
		x  +=   bias << mdig;
		val = fstb::bit_cast <T> (x);

		// Approximation of 1 + log2 (x) in [1 ; 2] -> [0 ; 1]
		val = poly (val);

		return val + T (log_2);
	}
}



template <typename T, typename P>
T	Approx::exp2_base (T val, P poly) noexcept
{
	if constexpr (! std::is_scalar_v <T>)
	{
		return val.exp2_base (poly);
	}

	else
	{
		static_assert (std::is_floating_point_v <T>, "Floating point only");

		// Truncated val for integer power of 2
		const auto     tx = floor_int (val);

		// Float remainder of power of 2
		val -= static_cast <T> (tx);

		// Approximation of 2^x in [0 ; 1]
		val = poly (val);

		// Add integer power of 2 to exponent
		typedef typename BitEquiv <T>::Int TI;
		auto           i = fstb::bit_cast <TI> (val);
		i += TI (tx) << (std::numeric_limits <T>::digits - 1);
		val = fstb::bit_cast <T> (i);
		assert (val >= 0);

		return val;
	}
}



}  // namespace fstb



#endif // fstb_Approx_transc_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
