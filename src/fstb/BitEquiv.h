/*****************************************************************************

        BitEquiv.h
        Author: Laurent de Soras, 2023

This class helps bit_cast-ing from a template. With it, it is possible to
retrieve the corresponding type, bitdepth, etc.

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_BitEquiv_HEADER_INCLUDED)
#define fstb_BitEquiv_HEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/def.h"
#include "fstb/Vf32.h"
#include "fstb/Vs32.h"

#include <cstdint>



namespace fstb
{



// Implementation detail
template <int S, int V>
class BitEquivN { };

template <>
class BitEquivN <32, 1>
{
public:
	static constexpr int _bits    = 32;
	static constexpr int _nbr_elt = 1;
	typedef int32_t Int;     // As signed integer
	typedef float   Flt;     // As floating point
	typedef Int     ScalInt; // As signed integer, scalar values
	typedef Flt     ScalFlt; // As floating point, scalar values
};

template <>
class BitEquivN <64, 1>
{
public:
	static constexpr int _bits    = 64;
	static constexpr int _nbr_elt = 1;
	typedef int64_t Int;
	typedef double  Flt;
	typedef Int     ScalInt;
	typedef Flt     ScalFlt;
};

template <>
class BitEquivN <32, 4>
{
public:
	static constexpr int _bits    = 32;
	static constexpr int _nbr_elt = 4;
	typedef Vs32    Int;
	typedef Vf32    Flt;
	typedef int32_t ScalInt;
	typedef float   ScalFlt;
};



template <typename T>
class BitEquiv : public BitEquivN <0, 0> { };

template <> class BitEquiv <float  > : public BitEquivN <32, 1> { };
template <> class BitEquiv <int32_t> : public BitEquivN <32, 1> { };
template <> class BitEquiv <double > : public BitEquivN <64, 1> { };
template <> class BitEquiv <int64_t> : public BitEquivN <64, 1> { };
template <> class BitEquiv <Vf32   > : public BitEquivN <32, 4> { };
template <> class BitEquiv <Vs32   > : public BitEquivN <32, 4> { };



}  // namespace fstb



//#include "fstb/BitEquiv.hpp"



#endif   // fstb_BitEquiv_HEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
