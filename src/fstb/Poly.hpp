/*****************************************************************************

        Poly.hpp
        Author: Laurent de Soras, 2021

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law. You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#if ! defined (fstb_Poly_CODEHEADER_INCLUDED)
#define fstb_Poly_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



namespace fstb
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



template <typename T, typename... C>
constexpr T	Poly::eval (T x, C... c) noexcept
{
	static_assert (
		sizeof... (C) >= 2, "Polynomial must have at least 2 coefficients."
	);

	/*** To do: check the best value for each supported architecture ***/
	if constexpr (sizeof... (C) < 6)
	{
		return horner (x, c...);
	}

	return estrin (x, c...);
}



template <class T>
constexpr T	Poly::horner (T x, T c0, T c1) noexcept
{
	return fma (c1, x, c0);
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2) noexcept
{
	return horner (x, c0, fma (c2, x, c1));
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2, T c3) noexcept
{
	return horner (x, c0, c1, fma (c3, x, c2));
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2, T c3, T c4) noexcept
{
	return horner (x, c0, c1, c2, fma (c4, x, c3));
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2, T c3, T c4, T c5) noexcept
{
	return horner (x, c0, c1, c2, c3, fma (c5, x, c4));
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2, T c3, T c4, T c5, T c6) noexcept
{
	return horner (x, c0, c1, c2, c3, c4, fma (c6, x, c5));
}

template <class T>
constexpr T	Poly::horner (T x, T c0, T c1, T c2, T c3, T c4, T c5, T c6, T c7) noexcept
{
	return horner (x, c0, c1, c2, c3, c4, c5, fma (c7, x, c6));
}



template <class T, int N>
constexpr T	Poly::horner (T x, const T (&c) [N]) noexcept
{
	if constexpr (N == 0)
	{
		return T (0);
	}

	auto           r = c [N - 1];
	for (int i = N - 2; i >= 0; --i)
	{
		r = fma (x, r, c [i]);
	}

	return r;
}



// Estrin evaluation is slightly less precise than Horner.
// Speed improvement starts with 6 coefficients (5 on ARM).
template <class T>
constexpr T	Poly::estrin (T x, T c0, T c1, T c2, T c3) noexcept
{
	const auto     x2  = x * x;
	const auto     r01 = fma (x, c1, c0);
	const auto     r23 = fma (x, c3, c2);
	return fma (x2, r23, r01);
}

template <class T>
constexpr T	Poly::estrin (T x, T c0, T c1, T c2, T c3, T c4) noexcept
{
	const auto     x2  = x * x;
	const auto     r12 = fma (x, c2, c1);
	const auto     r34 = fma (x, c4, c3);
	const auto     r14 = fma (x2, r34, r12);
	return fma (x, r14, c0);
}

template <class T>
constexpr T	Poly::estrin (T x, T c0, T c1, T c2, T c3, T c4, T c5) noexcept
{
	const auto     x2  = x * x;
	const auto     r01 = fma (x, c1, c0);
	const auto     r23 = fma (x, c3, c2);
	const auto     r45 = fma (x, c5, c4);
	const auto     r25 = fma (x2, r45, r23);
	return fma (x2, r25, r01);
}

template <class T>
constexpr T	Poly::estrin (T x, T c0, T c1, T c2, T c3, T c4, T c5, T c6) noexcept
{
	const auto     x2  = x  * x;
	const auto     x4  = x2 * x2;
	const auto     r01 = fma (x, c1, c0);
	const auto     r23 = fma (x, c3, c2);
	const auto     r45 = fma (x, c5, c4);
	const auto     r03 = fma (x2, r23, r01);
	const auto     r46 = fma (x2, c6, r45);
	return fma (x4, r46, r03);
}

template <class T>
constexpr T	Poly::estrin (T x, T c0, T c1, T c2, T c3, T c4, T c5, T c6, T c7) noexcept
{
	const auto     x2 = x  * x;
	const auto     x4 = x2 * x2;
	const auto     r01 = fma (x, c1, c0);
	const auto     r23 = fma (x, c3, c2);
	const auto     r45 = fma (x, c5, c4);
	const auto     r67 = fma (x, c7, c6);
	const auto     r03 = fma (x2, r23, r01);
	const auto     r47 = fma (x2, r67, r45);
	return fma (x4, r47, r03);
}



// Usually compiles to optimal code with -O3
// Based on a function by Vortico
// https://discord.com/channels/507604115854065674/507630527847596046/1073525377483419689
template <class T, int N>
constexpr T	Poly::estrin (T x, const T (&c) [N]) noexcept
{
	if constexpr (N == 0)
	{
		return T (0);
	}
	else if constexpr (N == 1)
	{
		return c [0];
	}

	constexpr auto M = (N + 1) / 2;
	T              b [M];
	for (int i = 0; i < M; ++i)
	{
		const auto     idx_even = 2 * i;
		b [i] = c [idx_even];
		const auto     idx_odd  = idx_even + 1;
		if (idx_odd < N)
		{
			b [i] = fma (x, c [idx_odd], b [i]);
		}
	}

	return estrin (x * x, b);
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// We don't use std::fma for scalars, as it could be very slow, depending on
// the platform.
template <class T>
constexpr T	Poly::fma (T x, T a, T b) noexcept
{
	return x * a + b;
}



}  // namespace fstb



#endif   // fstb_Poly_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
