/*****************************************************************************

        Approx_trigo.hpp
        Author: Laurent de Soras, 2024

--- Legal stuff ---

This program is free software. It comes without any warranty, to
the extent permitted by applicable law.You can redistribute it
and/or modify it under the terms of the Do What The Fuck You Want
To Public License, Version 2, as published by Sam Hocevar. See
http://www.wtfpl.net/ for more details.

*Tab=3***********************************************************************/



#pragma once
#if ! defined (fstb_Approx_trigo_CODEHEADER_INCLUDED)
#define fstb_Approx_trigo_CODEHEADER_INCLUDED



/*\\\ INCLUDE FILES \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/

#include "fstb/Poly.h"

#include <type_traits>

#include <cassert>
#include <cmath>



namespace fstb
{



/*\\\ PUBLIC \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*
f(x) ~ sin (x)
x in [-pi/2 ; pi/2]
Scaled formula
Max error:
32 bits: abs: 1.32e-07, rel: 1.33e-07
64 bits: abs: 2.41e-08, rel: 2.42e-08
*/

template <typename T>
constexpr T	Approx::sin_rbj (T x) noexcept
{
	const auto    a  = T ( 2.628441118718695e-06);
	const auto    b  = T (-1.982061326014539e-04);
	const auto    c  = T ( 0.008333224441393);
	const auto    d  = T (-0.166666657479818);
	const auto    x2 = x * x;

	return ((((a * x2 + b) * x2 + c) * x2 + d) * x2) * x + x;
}



/*
f(x) ~ cos (x)
x in [-pi ; pi]
Scaled formula
Max error:
32 bits: abs: 1.34e-07, rel: 1
64 bits: abs: 2.41e-08, rel: 2.42e-08
*/

template <typename T>
T	Approx::cos_rbj (T x) noexcept
{
	using std::abs;

	x = abs (x);

	const auto     hp = T (0.5 * PI);
	x = hp - x;

	return sin_rbj (x);
}



/*
f1 (x) ~ sin (x)
f2 (x) ~ cos (x)
x in [-3*pi ; 3*pi]
Scaled formula
*/

void	Approx::cos_sin_rbj (Vf32 &c, Vf32 &s, Vf32 x) noexcept
{
	const auto     hp  = Vf32 ( 0.5 * PI);
	const auto     hpm = Vf32 (-0.5 * PI);
	const auto     tp  = Vf32 ( 2   * PI);
	const auto     p   = Vf32 (       PI);
	const auto     pm  = Vf32 (      -PI);

	// x -> [-pi ; pi]
	x = restrict_angle_to_mpi_pi (x, pm, p, tp);

	auto           xs = x;

	// xs -> [-pi/2 ; pi/2]
	xs = restrict_sin_angle_to_mhpi_hpi (xs, hpm, hp, pm, p);

	auto           xc = x + hp;

	// xc -> [-pi ; pi]
	xc = select ((xc > p ), xc - tp, xc);

	// xc -> [-pi/2 ; pi/2]
	xc = restrict_sin_angle_to_mhpi_hpi (xc, hpm, hp, pm, p);

	s = sin_rbj (xs);
	c = sin_rbj (xc);
}



/*
f(x) ~ sin (x * pi/2)
x in [-1 ; 1]
Max error:
32 bits: abs: 1.92e-07, rel: 1.92e-07
64 bits: abs: 2.41e-08, rel: 2.42e-08
Original formula
*/

template <typename T>
constexpr T	Approx::sin_rbj_halfpi (T x) noexcept
{
	const auto     a  = T ( 0.0001530302);
	const auto     b  = T (-0.0046768800);
	const auto     c  = T ( 0.0796915849);
	const auto     d  = T (-0.6459640619);
	const auto     e  = T ( 1.5707963268);
	const auto     x2 = x * x;

	return Poly::horner (x2, e, d, c, b, a) * x;

	// Other coefficients found by Olli Niemitalo
	// Constaints: f(0) = 0, f(1) = 1, f'(1) = 0, odd-symmetry
	// https://dsp.stackexchange.com/questions/46629/finding-polynomial-approximations-of-a-sine-wave/46761#46761
	// 5th order, continuous derivative, peak harmonic distortion -78.99 dB
	//	   1.569778813, -0.6395576276, 0.06977881382
	// 7th order, continuous derivative, peak harmonic distortion -123.8368 dB
	//    1.570781972, -0.6458482979, 0.07935067784, -0.004284352588
	// 5th order, peak harmonic distortion -92.6 dB
	//    1.570034357, -0.6425216143, 0.07248725712
	// 7th order, peak harmonic distortion -133.627 dB
	//    1.5707953785726114835, -0.64590724797262922190, 0.079473610232926783079, -0.0043617408329090447344
}



/*
f(x) ~ sin (x * pi)
x in [-0.5 ; 1]
Max error:
32 bits: abs: 2.25e-07
64 bits: abs: 2.41e-08
*/

template <typename T>
constexpr T	Approx::sin_rbj_pi (T x) noexcept
{
	using std::abs;

	const auto     one   = T (1);
	const auto     two_x = one - abs (one - x - x);

	return sin_rbj_halfpi (two_x);
}



/*
f1(x) ~ sin (x * pi/2)
f2(x) ~ sin (x * pi)
x in [-0.5 ; 1]
Max error: abs: 1.92e-07
*/

std::array <float, 2>	Approx::sin_rbj_halfpi_pi (float x) noexcept
{
	const auto     xv = Vf32::set_pair (x, 1 - fabsf (1 - 2*x));
	const auto     yv = sin_rbj_halfpi (xv);
	return { yv.template extract <0> (), yv.template extract <1> () };
}



/*
f(x) ~ sin (x * pi/2)
x in [0 ; 1]
Formula by Andrew Simper
Max error:
32 bits: abs: 1.06e-07, rel: 1.06e-07
64 bits: abs: 1.16e-08, rel: 1.21e-08
Almost perfect for float (more accurate than sin_rbj_halfpi), but 1 more add.
*/

template <typename T>
constexpr T	Approx::sin_andy_halfpi (T x) noexcept
{
	const auto       c0 = T ( 0.5707963267948965801416385921);
	const auto       c1 = T (-0.6459636234636846741308116123);
	const auto       c2 = T ( 0.07968883060695860299455648319);
	const auto       c3 = T (-0.004672098002449504163232497889);
	const auto       c4 = T ( 0.00015056406427899515784903489630);

	const auto       x2 = x * x;
	const auto       u  = Poly::horner (x2, c0, c1, c2, c3, c4);
	const auto       y  = (u * x) + x;

	return y;
}



/*
f(x) ~ sin (x * pi)
x in [-1 ; 1]
Coefficients by Andrew Simper. Original idea by Colin Wallace.
https://discord.com/channels/507604115854065674/507630527847596046/1167749888654716959
Max error:
32 bits: abs: 2.72e-07, rel: 3.37e-07
64 bits: abs: 4.95e-09, rel: 5.13e-09
*/

template <typename T>
constexpr T	Approx::sin_andy_pi (T x) noexcept
{
	const auto     c0 = T ( 3.141592653589793160283277184);
	const auto     c1 = T (-2.026119488790393474558484669);
	const auto     c2 = T ( 0.5240356295457557460889623446);
	const auto     c3 = T (-0.07518486862624311135292900344);
	const auto     c4 = T ( 0.006856789848999791033358373984);
	const auto     c5 = T (-0.0003843877843722247904816234668);

	const auto     x2 = x * x;
	const auto     u  = x * Poly::eval (x2, c0, c1, c2, c3, c4, c5);
	// y  = u - u * x2; is slightly faster but causes a bigger relative error
	// near +/-1 (large in float, 8e-9 in double).
	const auto     y  = u * ((T (1) - x) * (T (1) + x));

	return y;
}



/* 
f(x) ~ sin (x * pi)
x in [-1 ; 1]
C3 continuity
Extrema are located at +/-0.5096 instead of +/-0.5 (phase distortion)
Max error: abs: 1.55 %, rel: 3.04 %
k may be adjusted between 1.75 and 2.0 (or even more). If extrema are supposed
to remain exactly at +/-1, the following formula can be used to compute sc
from k:
m  = sqrt ((3*k + 3 - sqrt (9 * k^2 - 2 * k + 9)) / 10)
sc = 1 / (m * (m^2 - k) * (m^2 - 1))
m is the position of the positive extremum.
k = 1.75 : extremas at +/-0.5, but relative error is bad. (sc = 16/9)
k = 1.875: best RMS absolute error
k = 1.967: best RMS relative error
k = 1.891: lowest maximum level of harmonics > 1 (-45.9 dB)
k = 2    : decent maximum relative error
https://www.desmos.com/calculator/8szfjzw3rz
*/

template <typename T>
constexpr T	Approx::sin_lds_pi (T x) noexcept
{
	const auto     k  = T (2.0);
	const auto     sc = T (1.52311310656647601124); // sqrt(411-41*sqrt(41))/8
	const auto     o  = T (1.0);

	const auto     x2 = x  * x;
	const auto     xs = x  * sc;
	const auto     p1 = x2 - o;
	const auto     p2 = x2 - k;
	const auto     y  = xs * p1 * p2;

	return y;
}



/*
The following "nick" approximations can be defined in its simplest expression
in the [-2 ; 2] range, equivalent to sin (x * pi/2):

	y = x * (2 - abs (x));
	z = d * (y * abs (y) - y) + y;

d is a curvature constant allowing error optimization:

	0.2240: maximum absolute error
	0.2175: maximum relative error
	0.2248: absolute error RMS
	0.2195: relative error RMS

The formula is equivalent to:

	t2 = fstb::sq (1 - abs (x));
	z  = std::copysign (Poly::horner (t2, 1, -1-d, d), x);

Same number of add, mul and signbit operations

Orignal ref:
Nicolas Capens, Fast and accurate sine/cosine, 2006-04-23
http://www.devmaster.net/forums/showthread.php?t=5784
*/

/*
f1(x) ~ sin (x)
x in [-pi ; pi]
Max error: abs: 0.919e-3, rel: 1.2e-2.
Relative error may be greater for values immediately close to +/-pi
d = 1 - pi/4 gives exact derivative at 0 but a max abs error of 2.787e-3
*/

template <typename T>
T	Approx::sin_nick (T x) noexcept
{
	assert (x >= T (-PI));
	assert (x <= T (+PI));

	const auto     b = T ( 4 /  PI);
	const auto     c = T (-4 / (PI * PI));
	const auto     d = T (0.224008);

	using std::abs;
	const auto     y = b * x + c * x * abs (x);
	const auto     z = d * (y * abs (y) - y) + y;

	return z;
}



/*
f1(x) ~ sin (x * 2 * pi)
x in [-0.5 ; 0.5]
Max error: 0.919e-3
*/

template <typename T>
T	Approx::sin_nick_2pi (T x) noexcept
{
	assert (x >= T (-0.5));
	assert (x <= T (+0.5));

	const auto     b = T (  8);
	const auto     c = T (-16);
	const auto     d = T (  0.224008);

	using std::abs;
	const auto     y = b * x + c * x * abs (x);
	const auto     z = d * (y * abs (y) - y) + y;

	return z;
}



// Retuns { cos, sin }
template <typename T>
std::array <T, 2>	Approx::cos_sin_nick_2pi (T x) noexcept
{
	assert (x >= T (-0.5));
	assert (x <= T (+0.5));

	using std::abs;

	// cos (x) = sin (x + pi/2)
	T              xc;
	if constexpr (std::is_scalar_v <T>)
	{
		xc = (x >= T (0.25)) ? x - T (0.75) : x + T (0.25);
	}
	else
	{
		const auto     c_025  = Vf32 (0.25f);
		const auto     c_075  = Vf32 (0.75f);
		const auto     ge_025 = (c_025 < x);
		xc = select (ge_025, x - c_075, x + c_025);
	}

	const auto     b = T (  8);
	const auto     c = T (-16);
	const auto     d = T (  0.224008);

	const auto     yc = b * xc + c * xc * abs (xc);
	const auto     ys = b * x  + c * x  * abs (x );
	const auto     zc = d * (yc * abs (yc) - yc) + yc;
	const auto     zs = d * (ys * abs (ys) - ys) + ys;

	return std::array <T, 2> { zc, zs };
}



/*
Relative errors:
below 0.01% up to pi/8
below 1.33% up to pi/4
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_taylor5 (T x) noexcept
{
	const auto     x_2 = x * x;
	const auto     c_3 = T (1.0 / 3);
	const auto     c_5 = T (2.0 / 15);

	return (c_5 * x_2 + c_3) * x_2 * x + x;
}



/*
Relative errors:
below 5.4e-7 up to pi/20 (4410 Hz @ Fs = 44.1 kHz)
below 2.3e-5 up to pi/8
below 2.9e-4 up to pi/4
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_7 (T x) noexcept
{
	const auto     x_2 = x * x;
	const auto     c_3 = T (1.0 / 3);
	const auto     c_5 = T (0.132);
	const auto     c_7 = T (0.0726);

	return Poly::horner (x_2, c_3, c_5, c_7) * (x_2 * x) + x;
}



/*
Relative errors:
below 4.3e-8 up to pi/20 (4410 Hz @ Fs = 44.1 kHz)
below 5.3e-6 up to pi/8
below 2e-5 up to pi/4
below 3.4 % up to 3*pi/8
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_9 (T x) noexcept
{
	const auto     x_2 = x * x;
	const auto     c_3 = T (1.0 / 3);
	const auto     c_5 = T (2.0 / 15);
	const auto     c_7 = T (0.050794);
	const auto     c_9 = T (0.034134);

	return Poly::horner (x_2, c_3, c_5, c_7, c_9) * (x_2 * x) + x;
}



/*
Formula by mystran, modified by LDS for a better accuracy the range end
https://www.kvraudio.com/forum/viewtopic.php?p=7491289#p7491289
tan x = sin (x) / cos (x)
      = sin (x) / sqrt (1 - sin (x) ^ 2)
Relative errors:
below 1e-8 up to pi/8
below 1.2e-6 up to pi/4 (double: 8.4e-7)
below 1e-5 up to 3*pi/8
below 1 % up to 0.9646 * pi/2
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
T	Approx::tan_mystran (T x) noexcept
{
	constexpr auto c3 = T (-1.0 / 6);
	constexpr auto c5 = T ( 1.0 / 120  - 1.00e-6);
	constexpr auto c7 = T (-1.0 / 5040 + 4.92e-6);

	const auto     x2 = x * x;
	const auto     s  = Poly::horner (x2, c3, c5, c7) * (x2 * x) + x;
	using std::sqrt;
	const auto     c  = T (sqrt (1 - s * s));

	return s / c;
}

Vf32	Approx::tan_mystran (Vf32 x) noexcept
{
	const auto     c1 = Vf32 ( 1         );
	const auto     c3 = Vf32 (-1.0 / 6   );
	const auto     c5 = Vf32 ( 1.0 / 120  - 1.00e-6);
	const auto     c7 = Vf32 (-1.0 / 5040 + 4.92e-6);

	const auto     x2 = x * x;
	const auto     s  = Poly::horner (x2, c3, c5, c7) * (x2 * x) + x;

	return s * (c1 - s * s).rsqrt ();
}



/*
Formula: Andrew Simper
https://discord.com/channels/507604115854065674/548502835608944681/872677465003274282
Relative error:
below 0.111 % up to 0.965 * pi/2
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_pade33 (T x) noexcept
{
//	x = limit (x, T (-1.54), T (1.54));
	const auto     x2  = x * x;
	constexpr auto d2  = 0.405097;
	const auto     num = T (0.075021 / d2) * x2 - T (1.00111 / d2);
	const auto     den =                     x2 - T (1.0     / d2);

	return x * num / den;
}



/*
Formula: Andrew Kay
https://andrewkay.name/blog/post/efficiently-approximating-tan-x/
Relative error:
below 0.175 % up to 0.999 * pi/2 (or the whole range in double)
below 1.6 % on the whole range (float)
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_t3a (T x) noexcept
{
	const auto     x2  = x * x;
	const auto     num = T (0.189759681063053) * x2 - T (2.471688400562703);
	const auto     den =                         x2 - T (2.4674011002723397);

	return x * num / den;
}



/*
Very high precision. Relative error is 1 % at 0.9993 * pi/2
PadeApproximant [Tan[x],{x,0,{5,5}}]
Relative error:
below 1.35e-8 up to pi/4 with a = 15
below 1 % up to 0.9993 * pi/2

If we use the identity tan (x) = sign (x) / tan (pi/2 - |x|), we can
make the function continuous (C1 at least) by using
d4 = (-967680 + 241920 * pi + 26880 * pi^2 - 1680 * pi^3 + pi^5) / (4 * pi^4)
   = 14.999975509385927280627711005255
Maximum relative error becomes 4.081e-9 on the whole ]-pi/4 ; pi/4[ range.
Actually the function behaves globally better on the whole range even when the
identity is not used. The relative error is larger below +/-0.7, but this is
not really a problem as the precision was unnecessary high near 0.
https://www.desmos.com/calculator/spsctmoyrb
*/

template <typename T>
constexpr T	Approx::tan_pade55 (T x) noexcept
{
//	const auto     d4  = T (15);
	const auto     d4  = T (14.999975509385927280627711005255);
	const auto     c0  = T (945);
	const auto     n2  = T (-105);
	const auto     d2  = T (-420);
	const auto     x2  = x * x;
	const auto     num = (     x2 + n2) * x2 + c0;
	const auto     den = (d4 * x2 + d2) * x2 + c0;

	return x * num / den;
}

/*
Coefficients for an order-7 Pade approximant:
-135135, 17325,  -378,  1
-135135, 62370, -3150, 28
Denominator modification for extended range (only for double precision):
-135135, 62370.00001, -3150.00005, 28.00004
Source:
https://www.wolframalpha.com/input?i=Pade+approximant+tan%28x%29+order+7%2C7
*/



/*
Formula by Jim Shima
http://dspguru.com/dsp/tricks/fixed-point-atan2-with-self-normalization/
Max error: +/-6.2e-3 rad
The original formula minimizes the average absolute error, so a coefficient
was tweaked to reduce the maximum error (1.015e-2 previously).

order-5 version:
	c5  = -0.0775572
	c3  =  0.287314
	c1  = -sum(c*)
	err = 8.4e-4
order-7 version:
	c7  =  0.0386378786305289
	c5  = -0.145916508504993
	c3  =  0.321088057777014
	c1  = -sum(c*)
	err = 1.2e-4

n = 1000
FindFit[
	Table[-ArcTan[x], {x, 0, 1, 1/n}],
	  (-Pi/4 - c3 - c5 - c7) ((x - 1)/n)
	+ c3 ((x - 1)/n)^3
	+ c5 ((x - 1)/n)^5
	+ c7 ((x - 1)/n)^7,
	{c7, c5, c3}, x
]
*/

template <typename T>
constexpr T	Approx::atan2_3th (T y, T x) noexcept
{
	constexpr auto c3 = T (0.18208); // Original formula: 0.1963
	constexpr auto c1 = T (-PI * 0.25) - c3;
	const auto     b  = atan2_beg (y, x);
	const auto     r  = b [0];
	const auto     c0 = b [1];
	const auto     r2 = r * r;

	auto           a  = ((c3 * r2) + c1) * r + c0;
	if (y < T (0))
	{
		a = -a;
	}

	return a;
}

Vf32	Approx::atan2_3th (Vf32 y, Vf32 x) noexcept
{
	const auto     c3 = Vf32 (0.18208f);
	const auto     c1 = Vf32 (PI * -0.25 - 0.18208);

	const auto     ys = y.signbit ();
	const auto     b  = atan2_beg (y, x);
	const auto     r  = b [0];
	const auto     c0 = b [1];
	const auto     r2 = r * r;
	auto           a  = c3;
	a  = fma (a, r2, c1);
	a  = fma (a, r , c0);
	a ^= ys;

	return a;
}



// Max error: +/-1.2e-4 rad
template <typename T>
constexpr T	Approx::atan2_7th (T y, T x) noexcept
{
	constexpr auto c7 = T ( 0.0386378786305289);
	constexpr auto c5 = T (-0.145916508504993);
	constexpr auto c3 = T ( 0.321088057777014);
	constexpr auto c1 = T (PI * -0.25 - 0.0386378786305289 - -0.145916508504993 - 0.321088057777014);

	const auto     b  = atan2_beg (y, x);
	const auto     r  = b [0];
	const auto     c0 = b [1];
	const auto     r2 = r * r;
	auto           a  = Poly::horner (r2, c1, c3, c5, c7) * r + c0;
	if (y < T (0))
	{
		a = -a;
	}

	return a;
}

Vf32	Approx::atan2_7th (Vf32 y, Vf32 x) noexcept
{
	const auto     c7 = Vf32 ( 0.0386378786305289f);
	const auto     c5 = Vf32 (-0.145916508504993f);
	const auto     c3 = Vf32 ( 0.321088057777014f);
	const auto     c1 = Vf32 (PI * -0.25 - 0.0386378786305289 - -0.145916508504993 - 0.321088057777014);

	const auto     ys = y.signbit ();
	const auto     b  = atan2_beg (y, x);
	const auto     r  = b [0];
	const auto     c0 = b [1];
	const auto     r2 = r * r;
	auto           a  = fma (Poly::horner (r2, c1, c3, c5, c7), r, c0);
	a ^= ys;

	return a;
}



/*\\\ PROTECTED \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



/*\\\ PRIVATE \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/



// [-3*pi ; 3*pi] -> [-pi ; pi]
// p = pi, pm = -pi, tp = 2*pi
Vf32	Approx::restrict_angle_to_mpi_pi (Vf32 x, const Vf32 &pm, const Vf32 &p, const Vf32 &tp) noexcept
{
	x = select ((x < pm), x + tp, x);
	x = select ((x > p ), x - tp, x);

	return x;
}



// [-pi ; pi] -> [-pi/2 ; pi/2]
// hpm = -pi/2, hp = pi/2, pm = -pi, p = pi
Vf32	Approx::restrict_sin_angle_to_mhpi_hpi (Vf32 x, const Vf32 &hpm, const Vf32 &hp, const Vf32 &pm, const Vf32 &p) noexcept
{
	x = select ((x < hpm), pm - x, x);
	x = select ((x > hp ), p  - x, x);

	return x;
}



template <typename T>
constexpr std::array <T, 2>	Approx::atan2_beg (T y, T x) noexcept
{
	constexpr T    c0p = T (PI * 0.25);
	constexpr T    c0n = T (PI * 0.75);
	constexpr T    eps = T (1e-10);

	const T        ya  = T (std::abs (y)) + eps;
	T              c0  = T (0);
	T              r   = T (0);
	if (x < T (0))
	{
		r  = (x + ya) / (ya - x);
		c0 = c0n;
	}
	else
	{
		r  = (x - ya) / (x + ya);
		c0 = c0p;
	}

	return std::array <T, 2> { r, c0 };
}

std::array <Vf32, 2>	Approx::atan2_beg (Vf32 y, Vf32 x) noexcept
{
	const auto     c0p  = Vf32 (PI * 0.25);
	const auto     c0n  = Vf32 (PI * 0.75);
	const auto     eps  = Vf32 (1e-10f);

	const auto     ya   = abs (y) + eps;
	const auto     xlt0 = x.is_lt_0 ();
	const auto     xpya = x + ya;
	const auto     xmya = x - ya;
	const auto     yamx = ya - x;
	const auto     c0   = select (xlt0, c0n, c0p);
	const auto     rnum = select (xlt0, xpya, xmya);
	const auto     rden = select (xlt0, yamx, xpya);
	const auto     r    = rnum / rden;

	return std::array <Vf32, 2> { r, c0 };
}



}  // namespace fstb



#endif // fstb_Approx_trigo_CODEHEADER_INCLUDED



/*\\\ EOF \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/
